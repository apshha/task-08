package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.*;
import com.epam.rd.java.basic.task8.entity.Flowers;
import com.epam.rd.java.basic.task8.util.Sorter;

public class Main {
	
	public static void main(String[] args) throws Exception {
		if (args.length != 1) {
			return;
		}
		
		String xmlFileName = args[0];
		System.out.println("Input ==> " + xmlFileName);
		
		////////////////////////////////////////////////////////
		// DOM
		////////////////////////////////////////////////////////
		
		// get container
		DOMController domController = new DOMController(xmlFileName);
		// PLACE YOUR CODE HERE
		domController.parse(true);
		Flowers flowers = domController.getFlowers();


		// sort (case 1)
		//Sorter.sortTariffsByName(tariffs);
		Sorter.sortFlowersByName(flowers);

		// save
		String outputXmlFile = "output.dom.xml";
		DOMController.saveToXML(flowers, outputXmlFile);

		////////////////////////////////////////////////////////
		// SAX
		////////////////////////////////////////////////////////

		// get
		SAXController saxController = new SAXController(xmlFileName);
		saxController.parse(true);
		flowers = saxController.getFlowers();
		// PLACE YOUR CODE HERE
		
		// sort  (case 2)
		Sorter.sortFlowersByAveLenFlower(flowers);
		
		// save
		outputXmlFile = "output.sax.xml";
		// PLACE YOUR CODE HERE
		DOMController.saveToXML(flowers, outputXmlFile);
		////////////////////////////////////////////////////////
		// StAX
		////////////////////////////////////////////////////////
		
		// get
		STAXController staxController = new STAXController(xmlFileName);
		staxController.parse();
		
		// sort  (case 3)
		Sorter.sortFlowersByWatering(flowers);
		
		// save
		outputXmlFile = "output.stax.xml";
		DOMController.saveToXML(flowers, outputXmlFile);

	}

}
