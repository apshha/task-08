package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.contstants.Constants;
import com.epam.rd.java.basic.task8.entity.*;
import org.w3c.dom.*;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

/**
 * Controller for DOM parser.
 */
public class DOMController {

    private final String xmlFileName;
    private Flowers flowers;

    public Flowers getFlowers() {
        return flowers;
    }

    public DOMController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parse(boolean validate) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        if (validate) {
            documentBuilderFactory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
            documentBuilderFactory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        documentBuilder.setErrorHandler(new DefaultHandler() {
            @Override
            public void error(SAXParseException e) throws SAXException {
                throw e;
            }
        });
        Document document = documentBuilder.parse(xmlFileName);
        Element root = document.getDocumentElement();
        flowers = new Flowers();
        NodeList flowerNodes = root.getElementsByTagName("flower");
        for (int i = 0; i < flowerNodes.getLength(); i++) {
            Flower flower = getFlower(flowerNodes.item(i));
            flowers.addFlower(flower);
        }

    }

    private Flower getFlower(Node qNode) {
        Flower flower = new Flower();
        Element qElement = (Element) qNode;
        Node qtNode = qElement.getElementsByTagName("name").item(0);
        flower.setName(qtNode.getTextContent());
        qtNode = qElement.getElementsByTagName("soil").item(0);
        flower.setSoil(qtNode.getTextContent());
        qtNode = qElement.getElementsByTagName("origin").item(0);
        flower.setOrigin(qtNode.getTextContent());
        flower.setVisualParameters(getVisualParameters(qElement));
        flower.setGrowingTips(getGrowingTips(qElement));
        qtNode = qElement.getElementsByTagName("multiplying").item(0);
        flower.setMultiplying(qtNode.getTextContent());
        return flower;
    }

    private GrowingTips getGrowingTips(Node qNode) {
        GrowingTips growingTips = new GrowingTips();
        Element qElement = (Element) qNode;
        growingTips.setTemperature(getTemperature(qElement));
        growingTips.setLighting(getLighting(qElement));
        growingTips.setWatering(getWatering(qElement));
        return growingTips;
    }

    private Watering getWatering(Node qNode) {
        Watering watering = new Watering();
        Element qElement = (Element) qNode;
        Node qtNode = qElement.getElementsByTagName("watering").item(0);
        watering.setMeasure(qtNode.getAttributes().item(0).getTextContent());
        watering.setValue(Integer.parseInt(qtNode.getTextContent()));
        return watering;
    }

    private Lighting getLighting(Node qNode) {
        Lighting lighting = new Lighting();
        Element qElement = (Element) qNode;
        Node qtNode = qElement.getElementsByTagName("lighting").item(0);
        lighting.setLightRequiring(qtNode.getAttributes().item(0).getTextContent());
        return lighting;
    }

    private Temperature getTemperature(Node qNode) {
        Temperature temperature = new Temperature();
        Element qElement = (Element) qNode;
        Node qtNode = qElement.getElementsByTagName("tempreture").item(0);
        temperature.setMeasure(qtNode.getAttributes().item(0).getTextContent());
        temperature.setValue(Integer.parseInt(qtNode.getTextContent()));
        return temperature;
    }

    private VisualParameters getVisualParameters(Node qNode) {
        VisualParameters visualParameters = new VisualParameters();
        Element qElement = (Element) qNode;
        Node qtNode = qElement.getElementsByTagName("stemColour").item(0);
        visualParameters.setStemColour(qtNode.getTextContent());
        qtNode = qElement.getElementsByTagName("leafColour").item(0);
        visualParameters.setLeafColour(qtNode.getTextContent());
        visualParameters.setAveLenFlower(getAveLenFlower(qElement));
        return visualParameters;
    }

    private AveLenFlower getAveLenFlower(Node qNode) {
        AveLenFlower aveLenFlower = new AveLenFlower();
        Element qElement = (Element) qNode;
        Node qtNode = qElement.getElementsByTagName("aveLenFlower").item(0);
        aveLenFlower.setValue(Integer.parseInt(qtNode.getTextContent()));
        aveLenFlower.setMeasure(qtNode.getAttributes().item(0).getTextContent());
        return aveLenFlower;
    }

    public static Document getDocument(Flowers flowers) throws ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setNamespaceAware(true);
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();
        Element flowersElement = document.createElement("flowers");
        flowersElement.setAttribute("xmlns", "http://www.nure.ua");
        flowersElement.setAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
        flowersElement.setAttribute("xsi:schemaLocation", "http://www.nure.ua input.xsd ");
        document.appendChild(flowersElement);
        for (Flower flower : flowers.getFlowers()) {
            //add flower
            Element flowerElement = document.createElement("flower");
            flowersElement.appendChild(flowerElement);
            //add name -> flower
            Element nameElement = document.createElement("name");
            nameElement.setTextContent(flower.getName());
            flowerElement.appendChild(nameElement);
            //add soil -> flower
            Element soilElement = document.createElement("soil");
            soilElement.setTextContent(flower.getSoil());
            flowerElement.appendChild(soilElement);
            //add origin -> flower
            Element originElement = document.createElement("origin");
            originElement.setTextContent(flower.getOrigin());
            flowerElement.appendChild(originElement);
            //add visualParameters -> flower
            Element visualParametersElement = document.createElement("visualParameters");
            flowerElement.appendChild(visualParametersElement);
            //add stemColour -> visualParameters
            Element stemColourElement = document.createElement("stemColour");
            stemColourElement.setTextContent(flower.getVisualParameters().getStemColour());
            visualParametersElement.appendChild(stemColourElement);
            //add leafColour -> visualParameters
            Element leafColourElement = document.createElement("leafColour");
            leafColourElement.setTextContent(flower.getVisualParameters().getLeafColour());
            visualParametersElement.appendChild(leafColourElement);
            //add aveLenFlower -> visualParameters
            Element aveLenFlowerElement = document.createElement("aveLenFlower");
            aveLenFlowerElement.setAttribute("measure", flower.getVisualParameters().getAveLenFlower().getMeasure());
            aveLenFlowerElement.setTextContent(String.valueOf(flower.getVisualParameters().getAveLenFlower()));
            visualParametersElement.appendChild(aveLenFlowerElement);
            //add growingTips -> flower
            Element growingTipsElement = document.createElement("growingTips");
            flowerElement.appendChild(growingTipsElement);
            //add tempreture -> growingTips
            Element temperatureElement = document.createElement("tempreture");
            temperatureElement.setAttribute("measure", flower.getGrowingTips().getTemperature().getMeasure());
            temperatureElement.setTextContent(String.valueOf(flower.getGrowingTips().getTemperature()));
            growingTipsElement.appendChild(temperatureElement);
            //add lighting -> growingTips
            Element lightingElement = document.createElement("lighting");
            lightingElement.setAttribute("lightRequiring", flower.getGrowingTips().getLighting().getLightRequiring());
            growingTipsElement.appendChild(lightingElement);
            //add watering -> growingTips
            Element wateringElement = document.createElement("watering");
            wateringElement.setAttribute("measure", flower.getGrowingTips().getWatering().getMeasure());
            wateringElement.setTextContent(String.valueOf(flower.getGrowingTips().getWatering()));
            growingTipsElement.appendChild(wateringElement);
            //add multiplying -> flower
            Element multiplyingElement = document.createElement("multiplying");
            multiplyingElement.setTextContent(flower.getMultiplying());
            flowerElement.appendChild(multiplyingElement);
        }
        return document;
    }

    public static void saveToXML(Flowers flowers, String xmlFileName) throws ParserConfigurationException, TransformerException {
        saveToXML(getDocument(flowers), xmlFileName);
    }

    public static void saveToXML(Document document, String xmlFileName) throws TransformerException {
        StreamResult result = new StreamResult(new File(xmlFileName));
        TransformerFactory tf = TransformerFactory.newInstance();
        javax.xml.transform.Transformer t = tf.newTransformer();
        t.setOutputProperty(OutputKeys.INDENT, "yes");
        t.transform(new DOMSource(document), result);
    }
}
