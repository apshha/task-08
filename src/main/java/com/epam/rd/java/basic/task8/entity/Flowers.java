package com.epam.rd.java.basic.task8.entity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class Flowers {
    private List<Flower> flowers;

    public List<Flower> getFlowers() {
        return flowers;
    }

    @Override
    public String toString() {
        return "Flowers{" +
                "flowers=" + flowers +
                '}';
    }

    public void addFlower(Flower newFlower) {
        if (Objects.isNull(flowers)) {
            flowers = new ArrayList<>();
        }
        flowers.add(newFlower);
    }
}
