package com.epam.rd.java.basic.task8.entity;


public class Watering {
    private String measure;
    int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    public String getMeasure() {
        return measure;
    }

    public void setMeasure(String measure) {
        this.measure = measure;
    }

    @Override
    public String toString() {
        return String.valueOf(value);
    }
}
