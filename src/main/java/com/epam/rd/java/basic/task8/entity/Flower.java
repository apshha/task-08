package com.epam.rd.java.basic.task8.entity;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String multiplying;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;


    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", visualParameters=" + visualParameters +
                ", growingTips=" + growingTips +
                ", multiplying=" + multiplying +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        if ("подзолистая".equals(soil) || "грунтовая".equals(soil) || "дерново-подзолистая".equals(soil)) {
            this.soil = soil;
        } else {
            throw new IllegalArgumentException();
        }
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        if ("листья".equals(multiplying) || "черенки".equals(multiplying) || "семена".equals(multiplying)) {
            this.multiplying = multiplying;
        } else {
            throw new IllegalArgumentException();
        }

    }
}
