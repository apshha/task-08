package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import javax.xml.transform.stream.StreamSource;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

    private String xmlFileName;
    private Flower flower;
    private Flowers flowers;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;


    public STAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public void parse() throws XMLStreamException {
        String currentElement = null;
        XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
        xmlInputFactory.setProperty(XMLInputFactory.IS_NAMESPACE_AWARE, true);
        XMLEventReader xmlEventReader = xmlInputFactory.createXMLEventReader(new StreamSource(xmlFileName));

        while (xmlEventReader.hasNext()) {
            XMLEvent xmlEvent = xmlEventReader.nextEvent();
            if (xmlEvent.isCharacters() && xmlEvent.asCharacters().isWhiteSpace()) {
                continue;
            }
            if (xmlEvent.isStartElement()) {
                StartElement startElement = xmlEvent.asStartElement();
                currentElement = startElement.getName().getLocalPart();
                if ("flowers".equals(currentElement)) {
                    flowers = new Flowers();
                }
                if ("flower".equals(currentElement)) {
                    flower = new Flower();
                }
                if ("visualParameters".equals(currentElement)) {
                    visualParameters = new VisualParameters();
                }
                if ("growingTips".equals(currentElement)) {
                    growingTips = new GrowingTips();
                }
                if ("aveLenFlower".equals(currentElement)) {
                    visualParameters.setAveLenFlower(new AveLenFlower());
                    visualParameters.getAveLenFlower().setMeasure(startElement.getAttributeByName(new QName("measure")).getValue());
                }
                if ("tempreture".equals(currentElement)) {
                    growingTips.setTemperature(new Temperature());
                    growingTips.getTemperature().setMeasure(startElement.getAttributeByName(new QName("measure")).getValue());
                }
                if ("lighting".equals(currentElement)) {
                    growingTips.setLighting(new Lighting());
                    growingTips.getLighting().setLightRequiring(startElement.getAttributeByName(new QName("lightRequiring")).getValue());
                }
                if ("watering".equals(currentElement)) {
                    growingTips.setWatering(new Watering());
                    growingTips.getWatering().setMeasure(startElement.getAttributeByName(new QName("measure")).getValue());
                }
            }
            if (xmlEvent.isCharacters()) {
                Characters characters = xmlEvent.asCharacters();
                String elementText = characters.getData();
                if ("name".equals(currentElement)) {
                    flower.setName(elementText);
                }
                if ("soil".equals(currentElement)) {
                    flower.setSoil(elementText);
                }
                if ("origin".equals(currentElement)) {
                    flower.setOrigin(elementText);
                }
                if ("stemColour".equals(currentElement)) {
                    visualParameters.setStemColour(elementText);
                }
                if ("leafColour".equals(currentElement)) {
                    visualParameters.setLeafColour(elementText);
                }
                if ("aveLenFlower".equals(currentElement)) {
                    visualParameters.getAveLenFlower().setValue(Integer.parseInt(elementText));
                }
                if ("tempreture".equals(currentElement)) {
                    growingTips.getTemperature().setValue(Integer.parseInt(elementText));
                }
                if ("watering".equals(currentElement)) {
                    growingTips.getWatering().setValue(Integer.parseInt(elementText));
                }
                if ("multiplying".equals(currentElement)) {
                    flower.setMultiplying(elementText);
                }
            }
            if (xmlEvent.isEndElement()) {
                EndElement endElement = xmlEvent.asEndElement();
                String localName = endElement.getName().getLocalPart();
                if ("flower".equals(localName)) {
                    flowers.addFlower(flower);
                }
                if ("visualParameters".equals(localName)) {
                    flower.setVisualParameters(visualParameters);
                }
                if ("growingTips".equals(localName)) {
                    flower.setGrowingTips(growingTips);
                }
            }
        }
        xmlEventReader.close();
    }

    public Flowers getFlowers() {
        return flowers;
    }
}