package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.contstants.Constants;
import com.epam.rd.java.basic.task8.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {

    private String xmlFileName;
    private String currentElement;
    private Flower flower;
    private Flowers flowers;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;


    public SAXController(String xmlFileName) {
        this.xmlFileName = xmlFileName;
    }

    public final void parse(final boolean validate) throws ParserConfigurationException, SAXException, IOException {
        SAXParserFactory saxParserFactory = SAXParserFactory.newInstance();
        saxParserFactory.setNamespaceAware(true);
        if (validate) {
            saxParserFactory.setFeature(Constants.FEATURE_TURN_VALIDATION_ON, true);
            saxParserFactory.setFeature(Constants.FEATURE_TURN_SCHEMA_VALIDATION_ON, true);
        }
        SAXParser saxParser = saxParserFactory.newSAXParser();
        saxParser.parse(xmlFileName, this);
    }

    @Override
    public void error(org.xml.sax.SAXParseException ex) throws SAXException {
        throw ex;
    }

    public Flowers getFlowers() {
        return flowers;
    }

    public void setFlowers(Flowers flowers) {
        this.flowers = flowers;
    }

    @Override
    public final void startElement(final String uri, final String localName, final String qName, final Attributes attributes) throws SAXException {
        currentElement = localName;
        if ("flowers".equals(currentElement)) {
            flowers = new Flowers();
            return;
        }
        if ("flower".equals(currentElement)) {
            flower = new Flower();
            return;
        }
        if ("visualParameters".equals(currentElement)) {
            visualParameters = new VisualParameters();
            flower.setVisualParameters(visualParameters);
            return;
        }
        if ("growingTips".equals(currentElement)) {
            growingTips = new GrowingTips();
            return;
        }
        if ("aveLenFlower".equals(currentElement)) {
            visualParameters.setAveLenFlower(new AveLenFlower());
            visualParameters.getAveLenFlower().setMeasure(attributes.getValue(0));
        }
        if ("tempreture".equals(currentElement)) {
            growingTips.setTemperature(new Temperature());
            growingTips.getTemperature().setMeasure(attributes.getValue(0));
        }
        if ("lighting".equals(currentElement)) {
            growingTips.setLighting(new Lighting());
            growingTips.getLighting().setLightRequiring(attributes.getValue(0));
        }
        if ("watering".equals(currentElement)) {
            growingTips.setWatering(new Watering());
            growingTips.getWatering().setMeasure(attributes.getValue(0));
        }
    }
    @Override
    public final void characters(final char[] ch, final int start, final int length) throws SAXException {
        String elementText = new String(ch, start, length).trim();
        if (elementText.isEmpty()) {
            return;
        }
        if ("name".equals(currentElement)) {
            flower.setName(elementText);
            return;
        }
        if ("soil".equals(currentElement)) {
            flower.setSoil(elementText);
            return;
        }
        if ("origin".equals(currentElement)) {
            flower.setOrigin(elementText);
            return;
        }
        if ("stemColour".equals(currentElement)) {
            visualParameters.setStemColour(elementText);
            return;
        }
        if ("leafColour".equals(currentElement)) {
            visualParameters.setLeafColour(elementText);
            return;
        }
        if ("aveLenFlower".equals(currentElement)) {
            visualParameters.getAveLenFlower().setValue(Integer.parseInt(elementText));
            return;
        }
        if ("tempreture".equals(currentElement)) {
            growingTips.getTemperature().setValue(Integer.parseInt(elementText));
            return;
        }
        if ("watering".equals(currentElement)) {
            growingTips.getWatering().setValue(Integer.parseInt(elementText));
            return;
        }
        if ("multiplying".equals(currentElement)) {
            flower.setMultiplying(elementText);
        }
    }

    @Override
    public final void endElement(final String uri, final String localName, final String qName) throws SAXException {
        if ("flower".equals(localName)) {
            flowers.addFlower(flower);
            return;
        }
        if ("visualParameters".equals(localName)) {
            flower.setVisualParameters(visualParameters);
            return;
        }
        if ("growingTips".equals(localName)) {
            flower.setGrowingTips(growingTips);
        }
    }
}