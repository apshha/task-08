package com.epam.rd.java.basic.task8.util;

import com.epam.rd.java.basic.task8.entity.Flower;
import com.epam.rd.java.basic.task8.entity.Flowers;

import java.util.Collections;
import java.util.Comparator;

public class Sorter {

    private Sorter() {
    }

    public static final Comparator<Flower> sortByName = new Comparator<Flower>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            String flower1 = o1.getName();
            String flower2 = o2.getName();
            return flower1.compareTo(flower2);
        }
    };
    public static final Comparator<Flower> sortByWatering = new Comparator<Flower>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            Integer flower1 = o1.getGrowingTips().getWatering().getValue();
            Integer flower2 = o2.getGrowingTips().getWatering().getValue();
            return flower1.compareTo(flower2);
        }
    };
    public static final Comparator<Flower> sortByAveLenFlower = new Comparator<Flower>() {
        @Override
        public int compare(Flower o1, Flower o2) {
            Integer flower1 = o1.getVisualParameters().getAveLenFlower().getValue();
            Integer flower2 = o2.getVisualParameters().getAveLenFlower().getValue();
            return flower1.compareTo(flower2);
        }
    };

    public static void sortFlowersByName(Flowers flowers) {
        Collections.sort(flowers.getFlowers(), sortByName);
    }

    public static void sortFlowersByWatering(Flowers flowers) {
        Collections.sort(flowers.getFlowers(), sortByWatering);
    }

    public static void sortFlowersByAveLenFlower(Flowers flowers) {
        Collections.sort(flowers.getFlowers(), sortByAveLenFlower);
    }
}
